Downloaded SugarCRM from http://sourceforge.net/projects/sugarcrm/

Sugar admin credentials: admin/admin

MySQL database credentials: root/mysql-server

Setup Schedulers with this crontab:
`* * * * *     cd /var/www/html/SugarCE-Full-6.5.21; php -f cron.php > /dev/null 2>&1`
