#!/bin/bash

# Delete previous version
rm -rf /var/www/html/SugarCE-Full-6.5.21
echo "drop database sugar;" | mysql -uroot -pmysql-server

# Setup everything
tar -xzf sugar-6.5.21.tgz
mv SugarCE-Full-6.5.21 /var/www/html
chown -R root:root /var/www/html
chmod 766 /var/www/html/SugarCE-Full-6.5.21/config.php
chmod 766 /var/www/html/SugarCE-Full-6.5.21/config_override.php
chmod 766 /var/www/html/SugarCE-Full-6.5.21/custom
chmod a+w -R /var/www/html/SugarCE-Full-6.5.21/cache
chmod a+w -R /var/www/html/SugarCE-Full-6.5.21/modules
chmod a+w -R /var/www/html/SugarCE-Full-6.5.21/upload

touch /var/www/html/SugarCE-Full-6.5.21/.htaccess
chmod a+w /var/www/html/SugarCE-Full-6.5.21/.htaccess
